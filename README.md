# Projet_DevOps_Part1
---

## Provisionning automatisé sur une infrastructure classique pour trois sites wordpress

Dans un premier temps, on vient crée 5 VagrantFile pour la génération des machines virtuelles

On a une liste de cinq machines:

- s0.infra => La machine hôte sur laquelle on exécutera les scripts de provisionning afin d'atteindre les machines invitées. s0 a une configuration réseau liée aux DNS, le DHCP et le Proxy (géré par le module HaProxy)

- s1.infra et s2.infra => ces deux machines sont les serveurs web qui hébergent les sites wordpress : 
1. devops.com
2. devsec.com
3. devsecops.com
s1 et s2 sont accompagnés par les services Apache et PHP pour la mise en service et le backend des sites wordpress.

- s3.infra => s3 se charge d'être le serveur de base de données collectant la data des sites wordpress. Elle est consitué du système de gestion de base de données MariaDB.

- s4.infra => s4 est le serveur NFS. Il partage un système de fichiers sur le réseau pour le stockage des données WordPress.

---

## Choix des technologies

Pour la création des machines initiales, l'outil choisi est Vagrant. À partir des fichiers VagrantFile on peut créer des configuration personnalisées et faire appel à des fichiers externes pour l'installation de packages. Dans notre cas, il s'agit d'Ansible et de Python3.

Pour l'automatisation de l'approvisionnement des machines, on est parti sur Ansible. Ansible est facile d'utilisation et plus versatile, notamment par rapport à l'aspect de l'utilisation d'agents, facilitant les connexions entre les machines hôtes et invités. De plus, l'intégration et l'écriture des scripts d'approvisionnements sont réalisés avec des langages lisibles sans grande difficulté (YAML).

---

## Scripts existants

Trois types de scripts sont présents pour le provisionning des machines:
- **playbook.yml** => Il s'agit du script principal référençant toutes les tâches qui vont être à effectuées pour l'alimentation des machines 
- **provision_s** *(n_machine)* .yml => Ces scripts sont ceux traités par le playbook d'Ansible pour lancer les commandes sur les machines cibles et les scripts shell associés 
- **provision_s** *(n_machine)* .sh => Les scripts shell contenant l'installation et la configuration des services des machines

Dans l'ordre, on va avoir trois actions: 
1. Lancement du playbook par la commande `ansible-playbook -i inventory playbook.yml`
2. **playbook.yml** => exécution des tâches en fonction des cibles <br>
    1. **provision_s** *(n_machine)* .yml => Fais la copie des fichiers shell vers la ou les machine(s) cible(s) <br>
    2. **provision_s** *(n_machine)* .sh => Lance les installations de paquets sur la ou les machine(s) cible(s)