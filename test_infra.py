import pytest
import testinfra
import requests

domaines = ['devops.com', 'devsec.com', 'devsecops.com']
urls = [f'http://{domaine}' for domaine in domaines]

# Définir les machines hôtes à tester
@pytest.fixture(scope="module")
def host(request):
    return testinfra.get_host("ansible://s0_infra")

# Test de ping
def test_ping(host):
    assert host.ping("s1_infra")
    assert host.ping("s2_infra")
    assert host.ping("s3_infra")
    assert host.ping("s4_infra")

# Test du DNS
def test_dns(host):
    assert host.check_output("nslookup s1_infra").startswith("Address: 192.168.50.11")
    assert host.check_output("nslookup s2_infra").startswith("Address: 192.168.50.12")
    assert host.check_output("nslookup s3_infra").startswith("Address: 192.168.50.13")
    assert host.check_output("nslookup s4_infra").startswith("Address: 192.168.50.14")

# Test sites WordPress
def test_sites_wordpress(host):
    for url in urls:
        try:
            response = requests.get(url)
            assert reponse.status_code == 200, f"{url} répond avec le code {response.status_code}."
            print(f'{url} est accessible.')
        except requests.RequestException as e:
            assert False, f"Erreur pour {url}: {e}"

# Test partage NFS 
def test_nfs(host):
    file_path = "/mnt/nfs/toto.txt"
    assert host.file(file_path).exists
    assert host.file(file_path).user == "vagrant"
    assert host.file(file_path).group == "vagrant"
    assert host.file(file_path).mode == 0o644

