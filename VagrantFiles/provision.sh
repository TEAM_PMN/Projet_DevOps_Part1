#!/bin/bash

# Vérifier si l'utilisateur a les privilèges root
if [ "$EUID" -ne 0 ]; then
    echo "Ce script doit être exécuté avec les privilèges root. Veuillez utiliser sudo ou exécuter en tant que root."
    exit 1
fi

# Installer SSH
apt update
apt install -y openssh-server

# Vérifier si l'installation de SSH s'est déroulée avec succès
if [ $? -eq 0 ]; then
    echo "SSH a été installé avec succès."
else
    echo "Une erreur s'est produite lors de l'installation de SSH."
    exit 1
fi

# Installer Python 3
apt install -y python3 python3-pip

# Vérifier si l'installation de Python 3 s'est déroulée avec succès
if [ $? -eq 0 ]; then
    echo "Python 3 a été installé avec succès."
else
    echo "Une erreur s'est produite lors de l'installation de Python 3."
    exit 1
fi

# Vérifier la version de Python 3 installée
python3_version=$(python3 --version 2>&1)
echo "Version de Python 3 installée : $python3_version"
