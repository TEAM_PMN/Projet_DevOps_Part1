#!/bin/bash

# Vérifier si l'utilisateur a les privilèges root
if [ "$EUID" -ne 0 ]; then
    echo "Ce script doit être exécuté avec les privilèges root. Veuillez utiliser sudo ou exécuter en tant que root."
    exit 1
fi

# Mettre à jour les listes de paquets
apt update

# Installer les dépendances
apt install -y software-properties-common

# Ajouter le dépôt Ansible
apt-add-repository --yes --update ppa:ansible/ansible

# Installer Ansible
apt install -y ansible

# Vérifier si l'installation s'est déroulée avec succès
if [ $? -eq 0 ]; then
    echo "Ansible a été installé avec succès."
else
    echo "Une erreur s'est produite lors de l'installation d'Ansible."
fi
