#!/bin/bash

# Installation de MariaDB
sudo apt-get update
sudo apt-get install -y mariadb-server

# Utilisation de s0_infra comme serveur DNS principal
sudo bash -c 'echo "nameserver 192.168.50.10 /n nameserver 8.8.8.8" > /etc/resolv.conf'

# Provisionnement terminé
touch /tmp/.provision_s3_infra
