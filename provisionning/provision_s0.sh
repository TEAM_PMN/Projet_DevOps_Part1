#!/bin/bash

# Installation de HAproxy pour le Load Balancer
sudo apt-get update
sudo apt-get install -y haproxy

# Configuration de HAproxy pour l'équilibrage de charge
cat <<EOF | sudo tee /etc/haproxy/haproxy.cfg
frontend web
  bind *:80
  mode http
  default_backend webservers

backend webservers
  balance roundrobin
  server s1_infra 192.168.50.11:80 check
  server s2_infra 192.168.50.12:80 check
EOF

# Installation de DNSmasq pour le serveur DNS
sudo apt-get install -y dnsmasq

# Configuration de DNSmasq pour le réseau interne
cat <<EOF | sudo tee /etc/dnsmasq.conf
interface=eth1
listen-address=192.168.50.10
bind-interfaces
dhcp-range=192.168.50.100,192.168.50.200,255.255.255.0,12h
EOF

# Redémarrage des services
sudo systemctl restart haproxy
sudo systemctl restart dnsmasq

# Utilisation de s0_infra comme serveur DNS principal
sudo bash -c 'echo "nameserver 192.168.50.10 /n nameserver 8.8.8.8" > /etc/resolv.conf'

# Provisionnement terminé
touch /tmp/.provision_s0_infra

