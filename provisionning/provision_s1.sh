#!/bin/bash

# Installation des dépendances pour le montage NFS
sudo apt-get update
sudo apt-get install -y nfs-common

# Partage NFS depuis s4_infra
sudo mkdir -p /mnt/nfs
sudo mount 192.168.50.14:/home/data /mnt/nfs

# Installation d'Apache et de PHP
sudo apt-get install -y apache2 php libapache2-mod-php

# Installation des extensions PHP requises pour WordPress
sudo apt-get install -y php-mysql php-curl php-gd php-intl php-mbstring php-soap php-xml php-xmlrpc php-zip

# Activation du module Apache pour la réécriture d'URL
sudo a2enmod rewrite

# Redémarrage d'Apache
sudo systemctl restart apache2

# Configuration des sites pour héberger les sites WordPress
sudo mkdir -p /var/www/devops.com /var/www/devsec.com /var/www/devsecops.com

# Récupération des fichiers wordpress
sudo wget https://wordpress.org/latest.tar.gz

# Copie des fichiers WordPress
sudo cp /home/vagrant/latest.tar.gz /var/www/devops.com/latest.tar.gz
sudo cp /home/vagrant/latest.tar.gz /var/www/devsec.com/latest.tar.gz
sudo cp /home/vagrant/latest.tar.gz /var/www/devsecops.com/latest.tar.gz

# Extraction des fichiers WordPress
sudo tar xzvf /var/www/devops.com/latest.tar.gz -C /var/www/devops.com/
sudo tar xzvf /var/www/devsec.com/latest.tar.gz -C /var/www/devsec.com/
sudo tar xzvf /var/www/devsecops.com/latest.tar.gz -C /var/www/devsecops.com/

sudo mv /var/www/devops.com/wordpress/* /var/www/devops.com/
sudo mv /var/www/devsec.com/wordpress/* /var/www/devsec.com/
sudo mv /var/www/devsecops.com/wordpress/* /var/www/devsecops.com/

# Suppression des fichiers inutiles
sudo rm /var/www/devops.com/wordpress/ -Rf
sudo rm /var/www/devsec.com/wordpress/ -Rf
sudo rm /var/www/devsecops.com/wordpress/ -Rf
sudo rm /var/www/devops.com/latest.tar.gz -Rf
sudo rm /var/www/devsec.com/latest.tar.gz -Rf
sudo rm /var/www/devsecops.com/latest.tar.gz -Rf

# Configuration des hôtes virtuels pour Apache
cat <<EOF | sudo tee /etc/apache2/sites-available/devops.com.conf
<VirtualHost *:80>
    ServerAdmin webmaster@localhost
    ServerName devops.com
    DocumentRoot /var/www/devops.com

    <Directory /var/www/devops.com>
        AllowOverride All
    </Directory>

    ErrorLog \${APACHE_LOG_DIR}/error.log
    CustomLog \${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
EOF

cat <<EOF | sudo tee /etc/apache2/sites-available/devsec.com.conf
<VirtualHost *:80>
    ServerAdmin webmaster@localhost
    ServerName devsec.com
    DocumentRoot /var/www/devsec.com

    <Directory /var/www/devsec.com>
        AllowOverride All
    </Directory>

    ErrorLog \${APACHE_LOG_DIR}/error.log
    CustomLog \${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
EOF

cat <<EOF | sudo tee /etc/apache2/sites-available/devsecops.com.conf
<VirtualHost *:80>
    ServerAdmin webmaster@localhost
    ServerName devsecops.com
    DocumentRoot /var/www/devsecops.com

    <Directory /var/www/devsecops.com>
        AllowOverride All
    </Directory>

    ErrorLog \${APACHE_LOG_DIR}/error.log
    CustomLog \${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
EOF

# Activation des sites virtuels pour Apache
sudo a2ensite devops.com.conf
sudo a2ensite devsec.com.conf
sudo a2ensite devsecops.com.conf

# Redémarrage d'Apache
sudo systemctl restart apache2

# Utilisation de s0_infra comme serveur DNS principal
sudo bash -c 'echo "nameserver 192.168.50.10 /n nameserver 8.8.8.8" > /etc/resolv.conf'

# Provisionnement terminé
touch /tmp/.provision_s1_infra

