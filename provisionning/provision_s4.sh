#!/bin/bash

# Installation du serveur NFS
sudo apt-get update
sudo apt-get install -y nfs-kernel-server

# Création du répertoire /home/data pour le partage NFS
sudo mkdir -p /home/data

# Configuration du partage NFS
cat <<EOF | sudo tee /etc/exports
/home/data 192.168.50.11(rw,sync,no_subtree_check)
/home/data 192.168.50.12(rw,sync,no_subtree_check)
EOF

# Redémarrage du service NFS
sudo exportfs -ra
sudo systemctl restart nfs-kernel-server

# Utilisation de s0_infra comme serveur DNS principal
sudo bash -c 'echo "nameserver 192.168.50.10 /n nameserver 8.8.8.8" > /etc/resolv.conf'

# Provisionnement terminé
touch /tmp/.provision_s4_infra

